package {  
    import flash.display.Sprite;	
    import flash.display.Shape;
    import flash.display.Stage;
    import flash.geom.Point;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard
    import flash.utils.*;

    public class Player extends SpaceObject{
        // A player's projectile bullet
        public var projectile:Projectile;
        // Credit goes to the internet for multiple key presses to work
        private var keyPressMapping:Object = new Object();
        // Can a projectile spawn?
        private var canSpawn:Boolean = true;
        // All of the player projectiles spawned
        public var allProjectiles:Array = new Array();
        // Number of maximum projectiles allowed
        private var maxBulletsAllowed:int = 5;
        
        // Add the ship and ship flame from library assets
        private var shipFlame:ShipFlame = new ShipFlame();
        private var ship:Ship = new Ship();
        
        public function Player(){
            drawPlayer();
        } 
        private function drawPlayer():void{
            addChild(ship);
            shipFlame.visible = false;
            ship.addChild(shipFlame);
            shipFlame.x = -18;
            
        }
        public function doPlayerAction():void{
            // Player actions can either be directional movements or shooting.
            if (keyPressMapping[Keyboard.UP]){
                shipFlame.visible = true;
                // Get the angular velocity and speed the ship up in the direction it's facing. 
                // We are in space so we never slow down passively.                
                this.vx += Math.cos(this.rotation*(Math.PI/180)) * this.speed;
                this.vy += Math.sin(this.rotation*(Math.PI/180)) * this.speed;                 
                // Make sure we stay within space speed limits
                switch (true)
                {
                    case (this.vx >= 5):
                    this.vx -= 0.1;
                    break;                    
                    case (this.vx <= -5):
                    this.vx += 0.1;
                    break;
                    case (this.vy >= 5):
                    this.vy -= 0.1;
                    break;
                    case (this.vy <= -5):
                    this.vy += 0.1;
                    break;
                }                
            }            
            if (keyPressMapping[Keyboard.LEFT]){
                this.rotation -= 5;
            }
            if (keyPressMapping[Keyboard.RIGHT]){
                this.rotation += 5;
            }
            
            if (keyPressMapping[Keyboard.SPACE] && canSpawn){
                shootProjectile();
            }
        }
        private function shootProjectile():void{
            // Rate limit on the number of bullets fired
            // TODO There is still something quirky going on here whilst holding spacebar.
            canSpawn = false;
            setTimeout(function():void{canSpawn=true}, 100)
            
            // Keep a reference of vx and vy so that our bullets are spawned in sync
            var vxProj = Math.cos(this.rotation*(Math.PI/180)) * this.speed;
            var vyProj = Math.sin(this.rotation*(Math.PI/180)) * this.speed;
            
            // Limit the number of bullets allowed on the screen
            // When the 6th bullet has been fired it replaces the first bullet fired
            if (allProjectiles.length === maxBulletsAllowed){
                // Update the first projectile with new values - it's being reused
                var firstProj:Projectile = allProjectiles.shift();
                firstProj.updateBullet(vxProj, vyProj, this.x, this.y);
                allProjectiles.push(firstProj);
            } else {
                projectile = new Projectile(vxProj, vyProj, this.x, this.y);
                allProjectiles.push(projectile);
                parent.addChild(projectile);
                
            }
        }
        public function clearProjectiles():void{
            // Remove any player projectiles from stage and empty array
            for each(var projectile:Projectile in allProjectiles){
                parent.removeChild(projectile);
            }
            allProjectiles = [];    
        }
        public function updateKeyUpPressMapping(e:KeyboardEvent):void{            
            delete keyPressMapping[e.keyCode];
            shipFlame.visible = false;            
        }
        public function updateKeyDownPressMapping(e:KeyboardEvent):void{
            keyPressMapping[e.keyCode] = true;
        }

    }
}