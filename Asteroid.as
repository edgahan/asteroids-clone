package {  
    public class Asteroid extends SpaceObject{   
        import flash.geom.Point;
            
        // An asteroid has a level (how tough it is) based on Level's stageNum.
        public var level:int;
        public var health:int;
        
        public function Asteroid(level:int){
            this.level = level;
            this.health = level;

            positionAndMoveAsteroid();
            drawAsteroid();
        } 
        private function drawAsteroid():void{
            this.graphics.beginFill(0xFFFFFF);
            this.graphics.drawCircle(1, 1, Math.random()*15+(25-level));
            this.graphics.endFill();                    
        }
        private function positionAndMoveAsteroid():void{
            // Don't position the asteroid next to player - it's not nice.
            var playerPoint:Point = new Point(250,250);
            var asteroidPoint:Point = new Point(Math.random()*500, Math.random()*500);
            var distanceBetweenPlayerAsteroid:Number = 
                Point.distance(playerPoint, asteroidPoint);
            if (distanceBetweenPlayerAsteroid < 150){
                positionAndMoveAsteroid()
            } else {
                this.x = asteroidPoint.x;
                this.y = asteroidPoint.y;
            }
            
            // Move asteroids randomly
            this.vx = Math.random()*level*0.4;
            this.vy = Math.random()*level*0.4;
        }
    }
}