package {  
    import flash.display.Sprite;	
    import flash.display.Shape;
    import flash.display.Stage;
    import flash.geom.Point;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard

    public class SpaceObject extends Sprite{
        // All space objects have a speed modifier and x, y velocities.
        public var speed:Number = 0.1;
        public var vy:Number = 0;
        public var vx:Number = 0;  
        
        public function SpaceObject(){
            // Because a reference to stage isn't always available we add an event listener         
            stage ? init() : addEventListener(Event.ADDED_TO_STAGE, init);
        } 
        private function init(e:Event=null):void{
            removeEventListener(Event.ADDED_TO_STAGE, init);
            positionObject(); 
        }
        public function positionObject(reset:Boolean = false):void{
            if (this.x === 0 && this.y === 0){
                this.x = stage.stageWidth/2;
                this.y = stage.stageHeight/2;
            }
            if (reset){
                this.x = stage.stageWidth/2;
                this.y = stage.stageHeight/2;
                this.vx = 0;
                this.vy = 0;
            }
        }
        public function updateObject():void{
            // The main gameLoop updates the SpaceObject x and y position
            this.x += this.vx;
            this.y += this.vy;
            // If the object reaches the borders of the game it wraps around
            if (this.x >= stage.stageWidth){
                this.x = 0;
            }
            if (this.x < 0){
                this.x = stage.stageWidth;
            }
            if (this.y >= stage.stageHeight){
                this.y = 0;
            }
            if (this.y < 0){
                this.y = stage.stageHeight;
            }
           
        }        
    }
}