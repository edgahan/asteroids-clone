package {
    import flash.display.MovieClip;   
    import flash.display.Sprite; 
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.text.TextField;
    import flash.text.TextFormat;
    import flash.text.TextFieldAutoSize;
    
    
    public class Level extends MovieClip{
        public var stageNum:Number = 1;
        public var points:Number = 0;
        // Create the point display for the level
        public var pointsText:TextField = new TextField();
        public var pointsTextFormat:TextFormat = new TextFormat();
        // Create the game over text for the level
        public var gameOverText:TextField = new TextField();
        public var gameOverTextFormat:TextFormat = new TextFormat();
        // Create the gameOver overlay
        private var overlay:Sprite = new Sprite();
        // A reference to the Main document class
        // TODO best to use this menthod instead?
        // public static function get instance():Main { return _instance; }
        private var parentMain:Object;
        
        // Create asteroid classes and hold all current asteroids in an array
        public var asteroid:Asteroid;
        public var allAsteroids:Array = new Array();
        
        public function Level(stageNum:Number = 1, points:Number = 0){
            this.stageNum = stageNum;
            this.points = points;
            stage ? init() : addEventListener(Event.ADDED_TO_STAGE, init);
        } 
        public function init(e:Event=null):void{
            // A reference to Main Document Class
            this.parentMain = parent;            
            // Show the points at the top
            setPointDisplay();
            // Add all the asteroids required for that level.
            setupAsteroids();            
        }
        public function gameOver():void{
            // Put a grey overlay over the game
            overlay.graphics.beginFill(0xCCCCCC);
            overlay.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            overlay.alpha = 0.6;
            // Add game over text
            gameOverText.text = "Game Over\nPlease insert more coins!";
            gameOverText.textColor = 0x000000;
            gameOverText.x = 60;
            gameOverText.y = 175;
            gameOverText.width = 375;
            gameOverText.height = 250;      
            gameOverText.autoSize = TextFieldAutoSize.CENTER;      
            gameOverTextFormat.size = 55;
            gameOverTextFormat.font = "Arial Bold";
            // TODO Is there a better way to style a textfield with different styles?
            // Maybe just two textfields would have been best.
            var breakMatch = gameOverText.text.match(/Please/).index;
            gameOverText.setTextFormat(gameOverTextFormat, 0, breakMatch) 
            gameOverTextFormat.size = 25;
            gameOverTextFormat.font = "Arial Bold";
            gameOverText.setTextFormat(gameOverTextFormat, breakMatch, gameOverText.text.length)
            // Add the textfield to overlay and the overlay to the stage
            overlay.addChild(gameOverText);
            addChild(overlay);
            removeChild(pointsText);
            // Remove overlay with any keyboard button and start the main timer again
            stage.addEventListener(KeyboardEvent.KEY_DOWN, function(e:KeyboardEvent):void{
                e.currentTarget.removeEventListener(e.type, arguments.callee);
                removeChild(overlay);
                addChild(pointsText);
                parentMain.timer.start();
            });
            
        }

        public function setPointDisplay():void{
            // Setup the points displayed on the top left
            addChild(pointsText)
            pointsText.text = 'Points ' + String(points) + '\nLevel: '+ stageNum;
            pointsText.textColor = 0xFFFF00;
            pointsText.x = 10;
            pointsText.y = 10;
            pointsText.width = 250;
            pointsText.height = 50;            
            pointsTextFormat.size = 20;
            pointsTextFormat.font = "Arial";
            pointsText.defaultTextFormat = pointsTextFormat;
            pointsText.setTextFormat(pointsTextFormat);
        }
        public function updatePointDisplay(gameOver:String = null):void{
            pointsText.text = 'Points ' + String(this.points) + '\nLevel: '+ stageNum;
        }
        public function setupAsteroids():void{
            var asteroidCount = this.stageNum;
            for (var i=0; i <= asteroidCount; i++){
                asteroid = new Asteroid(this.stageNum);
                allAsteroids.push(asteroid);
                addChild(asteroid);
            }
        }
    }
}