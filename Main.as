// Author: Eamonn Gahan
// Date: 15/06/2012
// Second Flash project :)


package {    
    import flash.display.MovieClip;	
    import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.events.Event;
    import flash.events.KeyboardEvent;
	
    public class Main extends MovieClip{
        
        // Level class, this contains asteroids and point display and gameOver screen
        public var level:Level;
        // Create the player (ship) class
        public var player:Player;
        // Create a game timer that updates SpaceObject movements
        public var timer:Timer;

        public function Main(){            
            // Make an instance of Level which will setup and asteroids and point display
            level = new Level();
            addChild(level);
            // Setup the player's ship
            setupPlayer();
            // Start the main timer loop (TODO: is ENTER_FRAME better than timer?)
            timer = new Timer(25);
            timer.addEventListener(TimerEvent.TIMER, gameLoop);
            timer.start();
        }
        private function gameLoop(e:Event):void {
            // Update the player's ship movements
            player.doPlayerAction();
            
            // Update all SpaceObjects instances and detect collisions
            updateAllObjects();
            detectCollisions();
            detectLevelChange();
        }
        private function detectLevelChange():void{
            // If there are no more asteroids start a harder level
            if (level.allAsteroids.length === 0){
                // Go to the next stage with carrying over our point score
                gotoStage(level.stageNum+=1, level.points);
            }
        }
        private function detectCollisions():void{
            // Detect collisions on the timer loop
            // TODO Check if AS3 hitObject detection is useable
            // TODO Or make it better with circle detection not square
            for each(var asteroid:Asteroid in level.allAsteroids){
                for each(var projectile:Projectile in player.allProjectiles){
                    // Is our projectile inside an asteroid?
                     if (projectile.x > asteroid.x - asteroid.width/2 && 
                        projectile.x < asteroid.x + asteroid.width/2 &&
                        projectile.y > asteroid.y - asteroid.height/2 && 
                        projectile.y < asteroid.y + asteroid.height/2){
                            // Register a hit with the asteroid and remove the projectile
                            asteroid.health -= 1;
                            // Lose a percentage of alpha based on total health (total health is the based on the stage)
                            asteroid.alpha = asteroid.alpha-(1.0/level.stageNum);
                            player.allProjectiles.splice(player.allProjectiles.indexOf(projectile), 1)
                            removeChild(projectile);
                            // Check if we should remove the asteroid from stage
                            if (asteroid.health === 0){
                                // Remove from stage and holder arrays
                                level.allAsteroids.splice(level.allAsteroids.indexOf(asteroid), 1)
                                level.removeChild(asteroid);
                                // Give points and update the points display
                                level.points += 50;
                                level.updatePointDisplay()                                
                            }
                        }
                 }
                 // Is the player's ship inside an asteroid?
                 if (player.x > asteroid.x - asteroid.width/2 && 
                    player.x < asteroid.x + asteroid.width/2 &&
                    player.y > asteroid.y - asteroid.height/2 && 
                    player.y < asteroid.y + asteroid.height/2){
                        // Game over man, game over. - Restart the game
                        gotoStage(level.stageNum = 1, level.points = 0);
                    }
            }
        }
        private function gotoStage(stageNum:Number, points:Number):void{
            // Remove the level, set new stage with specified points
            removeChild(level)
            level = new Level(stageNum, points)
            addChild(level)
            // If stageNum is 1 and points are 0 then it's a restart / game over.
            if (stageNum === 1 && points === 0){
                // Stop the timer
                timer.stop();
                // Show game over state.
                level.gameOver();
            }
            
            // Reposition the player
            player.positionObject(true);
            // Clear all the projectiles
            player.clearProjectiles();
        }
        private function updateAllObjects():void{
            // Update the player, projectile and asteroid movements
            player.updateObject();
            for (var i=0; i < player.allProjectiles.length; i++) {
                player.allProjectiles[i].updateObject();
            }
            for (var j=0; j < level.allAsteroids.length; j++) {
                level.allAsteroids[j].updateObject();
            }
        }
        private function setupPlayer():void{
            //Add the player to the stage and add event handlers for player actions.
            player = new Player();
            addChild(player);
            stage.addEventListener(KeyboardEvent.KEY_DOWN, player.updateKeyDownPressMapping);
            stage.addEventListener(KeyboardEvent.KEY_UP, player.updateKeyUpPressMapping);
        }         
    }
}