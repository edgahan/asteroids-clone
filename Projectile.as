package {    
    public class Projectile extends SpaceObject{
        private var projectileModifier:Number;
        public function Projectile(vx, vy, x, y){
            // Make projectiles fast
            updateBullet(vx, vy, x, y);
            drawBullet();
        } 
        private function drawBullet():void{
            this.graphics.beginFill(0xFFFFFF);
            this.graphics.drawCircle(1, 1, 2);
            this.graphics.endFill();                    
        }
        public function updateBullet(vx, vy, x, y):void{
            // Takes a base set of coordinates from the ship and updates bullet trajectory
            projectileModifier = 75;
            this.x = x;
            this.y = y;            
            this.vx = vx*projectileModifier;
            this.vy = vy*projectileModifier;
        }
    }
}